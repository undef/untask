#http://python-gtk-3-tutorial.readthedocs.org/

from gi.repository import Gtk

builder = Gtk.Builder()
builder.add_from_file("gui.glade")

window = builder.get_object("appWindow")
window.connect("delete-event", Gtk.main_quit)
window.show_all()

Gtk.main()